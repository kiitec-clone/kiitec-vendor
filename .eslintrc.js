module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ['plugin:vue/essential'],
  rules: {
    'no-console': 'error',
    'no-debugger': 'error',
    quotes: [2, 'single', { 'avoidEscape': true, 'allowTemplateLiterals': true }]
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
};