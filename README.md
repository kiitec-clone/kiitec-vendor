# Kiitec Client

## Requirements

Make sure you have the following installed on your system.
- nodejs >= v12
- npm >= 6.9.0

## Project setup

- `git clone https://gitlab.com/kiitec.afaara/vendor.git`
- `cd vendor`
- create a `.env.local` file, copy all text in `.env.local.example` into the `.env.local` file and fill in your configurations.
- `npm install`
- `npm run serve`

## More Info

#### Compiles and hot-reloads for development

```
npm run serve
```

#### Compiles and minifies for production

```
npm run build
```

#### Run your tests

```
npm run test
```

#### Lints and fixes files

```
npm run lint
```

#### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
