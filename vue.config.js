module.exports = {
	css: {
		loaderOptions: {
			sass: {
				data: `@import "@/scss/app.scss";`,
			},
		},
	},
	devServer: {
		port: process.env.PORT || 8080,
		disableHostCheck: true,
	},
};
