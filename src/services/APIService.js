import axios from 'axios';
import store from '../store';
import TokenService from './TokenService';
import {LOGOUT} from '../store/actionTypes';

const APIService = {
	init(baseURL) {
		axios.defaults.baseURL = baseURL;
	},
	setAuthHeader() {
		axios.defaults.headers.common.Authorization = `Bearer ${TokenService.getToken()}`;
	},
	removeAuthHeader() {
		axios.defaults.headers.common = {};
	},
	get: url => axios.get(url),
	post: (url, data) => axios.post(url, data),
	put: (url, data) => axios.put(url, data),
	patch: url => axios.patch(url),
	delete: url => axios.delete(url),
	makeRequest: requestData => axios(requestData),

	_401Interceptor: null,
	_APIResponseInterceptor: null,

	mount401Interceptor() {
		this._401Interceptor = axios.interceptors.response.use(response => response, async (error) => {
			if (error.request.status === 401) {
				if (!error.config.url.includes('/token')) {
					await store.dispatch(LOGOUT);

					throw error;
				} else {
					// We can try to refresh the token
				}
			}

			throw error;
		})
	},
	unmount401Interceptor() {
		axios.interceptors.response.eject(this._401Interceptor);
	}
};

export default APIService;
