import APIService from './APIService';

export default class OrderService {
  static async all() {
    const response = await APIService.get(`/vendor/orders`);
    return response.data.data;
  }

  static async accept(id) {
    const response = await APIService.patch(`/vendor/orders/accept/${id}`);
    return response.data.data;
  }

  static async decline(id) {
    const response = await APIService.patch(`/vendor/orders/decline/${id}`);
    return response.data.data;
  }
}
