import APIService from './APIService';
import TokenService from './TokenService';
import AuthenticationError from '../classes/AuthenticationError';

export default class AuthService {
	static async login(email, password) {
		const requestData = {
			method: 'post',
			url: '/login',
			data: {
				grant_type: 'password',
				type: 'vendor',
				email,
				password,
			},
			auth: {
				username: process.env.VUE_APP_CLIENT_ID,
				password: process.env.VUE_APP_CLIENT_SECRET,
			}
		};

		try {
			const response = await APIService.makeRequest(requestData);

			TokenService.saveToken(response.data.data.token);
			APIService.setAuthHeader();

			return response.data.data;
		} catch (e) {
			throw new AuthenticationError(e.response.status, e.response.data.message, e.response.data.errors);
		}
	}

	static async register(data) {
		const requestData = {
			method: 'post',
			url: '/vendor/create',
			data,
		};

		try {
			const response = await APIService.makeRequest(requestData);
			TokenService.saveToken(response.data.data.token);
			APIService.setAuthHeader();

			APIService.mount401Interceptor();

			return response.data.data;
		} catch (e) {
			throw new AuthenticationError(e.response.status, e.response.data.message, e.response.data.errors);
		}
	}

	static logout() {
		TokenService.removeToken();
		APIService.removeAuthHeader();
		APIService.unmount401Interceptor();
	}

	static async getAuthUser() {
		try {
			const response = await APIService.get('/user');
			return response.data.data;
		} catch (e) {
			throw e;
		}
	}

	static async getAuthVendor() {
		try {
			const response = await APIService.get('/vendor');
			return response.data;
		} catch (e) {
			throw e;
		}
	}
}
