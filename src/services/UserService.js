import APIService from './APIService';

export default class UserService {
    static async isBoarded() {
        const  response = await APIService.get(`/vendor/onboarding-status`);
        return response.data.data.isBoarded;
    }

    static async isApproved() {
        const  response = await APIService.get(`/vendor/approval-status`);
        return response.data.data.isApproved;
    }

    static async transactions() {
        const response = await APIService.get('/vendor/transactions')
        return response.data.data;
    }

    static async withdraw(amount) {
        const data = {
            amount
        }
        try {
        const response = await APIService.post('/vendor/withdraw', data);
        return response.data.data;
        }catch (err) {
            return err.data
        }
    }
}
