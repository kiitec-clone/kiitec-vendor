const TOKEN_IDENTIFIER = 'kiitec_vendor_token';
const TokenService = {
	getToken: () => localStorage.getItem(TOKEN_IDENTIFIER),
	saveToken: (accessToken) => localStorage.setItem(TOKEN_IDENTIFIER, accessToken),
	removeToken: () => localStorage.removeItem(TOKEN_IDENTIFIER),
};

export default TokenService;
