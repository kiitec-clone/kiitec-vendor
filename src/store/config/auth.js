// import { APIService, API_KEY } from '@/config';
import axios from 'axios'
import APIService from '../../services/APIService';

const auth = {
  login,
  register,
  getCountries,
  getStates,
  getCities,
  getIndustries,
  getLocalGovernments,
  logout
};

function login(credentials) {
  return APIService
    .post('/login', credentials)
    .then(resp => {
      return resp.data;
    })
    .catch(err => {
      return { error: err.response.data.errors, type: 'error' }
    });
}

function register(credentials) {
  return APIService
    .post('/vendor/create', credentials)
    .then(resp => {
      return resp.data;
    })
    .catch(err => {
      return {
        error: err.response.data.error,
        type: 'error',
        message: err.response.data.message
      }
    });
}

function getCountries() {
  return APIService
    .get('/countries')
    .then(resp => {
      return resp.data.data;
    })
}

function getStates(payload) {
  return APIService
    .get(`/countries/${payload}/states`)
    .then(resp => {
      return resp.data.data;
    });
}

function getCities(payload) {
  return APIService
    .get(`/states/${payload}/cities`)
    .then(resp => {
      return resp.data.data;
    })
}
function getLocalGovernments(payload) {
  return APIService
    .get(`/states/${payload}/local_governments`)
    .then(resp => {
      return resp.data.data;
    })
}

function getIndustries() {
  return APIService
    .get('/categories')
    .then(resp => {
      return resp.data;
    })
}

function logout() {
  return APIService.post('/logout').then(resp => {
    localStorage.removeItem('token');
    return resp.data;
  });
}

function verifyEmail(id) {
  return APIService
    .get(`/vendor/verify/${id}`)
    .then(resp => {
      return resp.data;
    })
}

export default auth;
