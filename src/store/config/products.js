import APIService from '../../services/APIService';

const product = {
  getVendorProducts,
  getAllProducts,
  getSingleProduct,
  createProduct,
  deleteProduct,
  updateProduct,
  deleteProductImage,
  uploadProductImages,
  getProductTags
};

function getVendorProducts() {
  return APIService.get('/vendor/products')
    .then(resp => {
      return resp.data;
    })
    .catch(err => {
      return { error: err.response.data.error, type: 'error' };
    });
}

function getAllProducts() {
  return APIService.get('/product/all')
    .then(resp => {
      return resp.data;
    })
    .catch(err => {
      return { error: err.response.data.error, type: 'error' };
    });
}

function getSingleProduct(payload) {
  return APIService.get(`/vendor/products/${payload}`)
    .then(resp => {
      return resp.data;
    })
    .catch(err => {
      return { error: err.response.data.error, type: 'error' };
    });
}

function createProduct(payload) {
  return APIService.post('/vendor/products', payload)
    .then(resp => {
      return resp.data;
    })
    .catch(err => {
      return { error: err.response.data.error, type: 'error' };
    });
}

function deleteProduct(payload) {
  return APIService.delete(`/vendor/products/${payload}`)
    .then(resp => {
      return resp.data;
    })
    .catch(err => {
      return { error: err.response.data.error, type: 'error' };
    });
}

function updateProduct(payload) {
  return APIService.put(`/vendor/products/${payload.id}`, payload.data)
    .then(resp => {
      return resp.data;
    })
    .catch(err => {
      return { error: err.response.data.error, type: 'error' };
    });
}

function deleteProductImage(payload) {
  return APIService.delete(`/vendor/products/images/${payload}`)
    .then(resp => {
      return resp.data;
    })
    .catch(err => {
      return { error: err.response.data.error, type: 'error' };
    });
}

function uploadProductImages(payload) {
  return APIService.post('/vendor/products/images', payload)
    .then(resp => {
      return resp.data;
    })
    .catch(err => {
      return { error: err.response.data.error, type: 'error' };
    });
}
function getProductTags() {
  return APIService.get('/products/tags')
    .then(resp => {
      return resp.data;
    })
    .catch(err => {
      return { error: err.response.data.error, type: 'error' };
    });
}

export default product;
