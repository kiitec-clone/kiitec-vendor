import APIService from '../../services/APIService';

const customization = {
  getAllCustomizations: getAllCustomizations,
  addCustomization,
  getCustomization,
  addCustomizationFeature,
  addFeatureOption,
  removeFeatureOption,
  deleteCustomizationFeature,
  archiveCustomization,
  updateCustomization,
};

function getAllCustomizations() {
  return APIService.get('/vendor/customization').then(resp => {
    return resp.data;
  });
}

function getCustomization(customizationId) {
  return APIService.get(`/vendor/customization/${customizationId}`).then(resp => {
    return resp.data;
  });
}

function addCustomization(customizationData) {
  return APIService.post('/vendor/customization', customizationData).then(resp => {
    return resp.data;
  });
}
function updateCustomization(customizationData) {
  return APIService.put(`/vendor/customization/${customizationData.id}`, customizationData.newData).then(resp => {
    return resp.data;
  });
}

function addCustomizationFeature(featureData) {
  return APIService.post('/vendor/features', featureData).then(resp => {
    return resp.data;
  });
}

function addFeatureOption(optionData) {
  return APIService.post('/vendor/options', optionData).then(resp => {
    return resp.data;
  });
}
function removeFeatureOption(optionData) {
  return APIService.put(`/vendor/options/${optionData.feature_id}`, optionData).then(resp => {
    return resp.data;
  });
}
function deleteCustomizationFeature(featureId) {
  return APIService.delete(`/vendor/features/${featureId}`).then(resp => {
    return resp.data;
  });
}

function archiveCustomization(customizationId) {
  return APIService.delete(`/vendor/customization/${customizationId}`).then(resp => {
    return resp.data;
  });
}


export default customization;
