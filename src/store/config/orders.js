import api from '@/config';

const user = {
  getOrders
};

function getOrders() {
  return api.get('/vendor/order').then(resp => {
    return resp.data;
  });
}


export default user;
