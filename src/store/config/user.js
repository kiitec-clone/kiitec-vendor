import APIService from '../../services/APIService';

const user = {
  getUser,
  getVendor,
  updateUser,
  checkOnboard,
  completeOnboard,
  vendorOnboard,
  vendorOnboardUpdate,
};

function getUser() {
  return APIService.get('/user').then(resp => {
    return resp.data;
  });
}

function getVendor() {
  return APIService.get('/vendor').then(resp => {
    return resp.data;
  });
}

function updateUser(payload) {
  return APIService
    .put(`/vendor/update/${payload.id}`, payload)
    .then(resp => {
      return resp.data;
    })
    .catch(err => {
      return { error: err.response.data.error, type: 'error' }
    });
}

function checkOnboard(payload) {
  return APIService.get(`/vendor/onboarding-status`).then(resp => {
    return resp.data.data.isBoarded;
  })
}

function completeOnboard(payload) {
  return APIService.get(`/vendor/${payload}/onboarding-complete`).then(resp => {
    return resp.data
  })
}

function vendorOnboard(payload) {
  return APIService.post(`/vendor/onboarding`, payload.data).then(resp => {
    return resp.data
  })
    .catch(err => {
      return { error: err.response.data.error, type: 'error' }
    });
}

function vendorOnboardUpdate(payload) {
  return APIService.post(`/vendor/onboarding-update`, payload.data).then(resp => {
    return resp.data
  })
    .catch(err => {
      return { error: err.response.data.error, type: 'error' }
    });
}

export default user;
