import Vue from 'vue';
import Vuex from 'vuex';
// import auth from './modules/auth';
import user from './modules/user';
import products from './modules/products';
import orders from './modules/orders';
import entries from './modules/entries';
import {auth} from './modules/authentication';
import register from './modules/register';
import {notifications} from './modules/notifications';
import customization from './modules/customizations';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
    // auth,
    user,
    products,
    orders,
    entries,
    auth,
    register,
    notifications,
    customization,
  },
  strict: debug
});
