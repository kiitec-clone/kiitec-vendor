import * as action from '../actionTypes';
import * as mutation from '../mutationTypes';

import customization from '../config/customization';

const state = {
  customizations: [],
  currentCustomization: {},
};

const getters = {
  getCustomizations: state => {
    return state.customizations;
  },
  getCurrentCustomization: state => {
    return state.currentCustomization;
  },
};

const actions = {
  [action.GET_ALL_CUSTOMIZATIONS]({ commit }) {
    return customization
      .getAllCustomizations()
      .then(resp => {
        if (resp.type == 'error') {
          return resp
        } else {
          commit(mutation.SET_CUSTOMIZATIONS, resp.data);
          return true;
        }
      })
  },
  [action.ADD_CUSTOMIZATION]({ commit }, customizationData) {
    return customization
      .addCustomization(customizationData)
      .then(resp => {
          return resp.data
      });
  },
  [action.UPDATE_CUSTOMIZATION]({ commit }, customizationData) {
    return customization
      .updateCustomization(customizationData)
      .then(resp => {
          return resp.data
      });
  },
  [action.ADD_CUSTOMIZATION_FEATURE]({ commit, dispatch }, featureData) {
    return customization
      .addCustomizationFeature(featureData)
      .then(resp => {
          dispatch(action.GET_CUSTOMIZATION, featureData.get('customization_id'));
          return resp.data
      });
  },
  [action.ADD_FEATURE_OPTION]({ commit, dispatch }, optionData) {
    return customization
      .addFeatureOption(optionData)
      .then(resp => {
          return resp.data
      });
  },
  [action.REMOVE_FEATURE_OPTION]({ commit, dispatch }, optionData) {
    return customization
      .removeFeatureOption(optionData)
      .then(resp => {
          return resp.data
      });
  },
  [action.REMOVE_CUSTOMIZATION_FEATURE]({ commit, dispatch }, data) {
    return customization
      .deleteCustomizationFeature(data.feature.id)
      .then(resp => {
          return resp.status === 'success'
      });
  },
  [action.ARCHIVE_CUSTOMIZATION]({ commit, dispatch }, customizationId) {
    return customization
      .archiveCustomization(customizationId)
      .then(resp => {
          return resp.status === 'success'
      });
  },
  [action.GET_CUSTOMIZATION]({ commit }, customizationId) {
    return customization
      .getCustomization(customizationId)
      .then(resp => {
          commit(mutation.SET_CURRENT_CUSTOMIZATION, resp.data);
          return resp.data
      });
  }
};

const mutations = {
  [mutation.SET_CUSTOMIZATIONS](state, customizations) {
    state.customizations = customizations;
  },
  [mutation.SET_CURRENT_CUSTOMIZATION](state, customization) {
    state.currentCustomization = customization;
  },
};

export default {
  state,
  getters,
  mutations,
  actions
};
