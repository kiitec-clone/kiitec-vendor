import * as action from "../actionTypes";
import * as mutation from "../mutationTypes";

import OrderService from "@/services/OrderService";

const state = {
  entries: [],
};

const getters = {
  getEntries: (state) => {
    return state.entries;
  },
};

const actions = {
  [action.GET_ENTRIES]({ commit }) {
    // const response = await OrderService.all();
    // commit(mutation.SET_ORDERS, response)
    //       return true;
    return OrderService.all().then((resp) => {
      commit(mutation.SET_ENTRIES, resp);
      return resp;
    });
  },
  [action.ACCEPT_ENTRY]({ commit }, payload) {
    // const response = await OrderService.all();
    // commit(mutation.SET_ORDERS, response)
    //       return true;
    return OrderService.accept(payload)
      .then((resp) => {
        commit(mutation.ACCEPT_ENTRY, payload);
        return true;
      })
      .catch((e) => {
        return false;
      });
  },

  [action.DECLINE_ENTRY]({ commit }, payload) {
    // const response = await OrderService.all();
    // commit(mutation.SET_ORDERS, response)
    //       return true;
    return OrderService.decline(payload)
      .then((resp) => {
        commit(mutation.DECLINE_ENTRY, payload);
        return true;
      })
      .catch((e) => {
        return false;
      });
  },
};

const mutations = {
  [mutation.SET_ENTRIES](state, payload) {
    state.entries = payload;
  },
  [mutation.ACCEPT_ENTRY](state, payload) {
    const itemIndex = state.entries.findIndex((item) => item.id === payload);
    const entry = state.entries[itemIndex];
    entry.status = "accepted";
    state.entries.splice(itemIndex, 1, entry);
  },
  [mutation.DECLINE_ENTRY](state, payload) {
    const itemIndex = state.entries.findIndex((item) => item.id === payload);
    const entry = state.entries[itemIndex];
    entry.status = "declined";
    state.entries.splice(itemIndex, 1, entry);
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
