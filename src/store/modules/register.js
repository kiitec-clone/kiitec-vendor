import * as action from '../actionTypes';
import * as mutation from '../mutationTypes';

import auth from '../config/auth';

const state = {
    verifyEmailId: '',
    countries: [],
    states: [],
    cities: [],
    localGovernments: [],
    industries: [],
    lgas: [],
    storeLgas: []
};

const getters = {
    getEmailId: state => {
        return state.verifyEmailId;
    },
    getCountries: state => {
        return state.countries;
    },
    getStates: state => {
        return state.states;
    },
    getCities: state => {
        return state.cities;
    },
    getLocalGovernments: state => {
        return state.localGovernments;
    },
    getIndustries: state => {
        return state.industries;
    },
    getLGAS: state => {
        return state.lgas;
    },
    getStoreLGAS: state => {
        return state.storeLgas;
    }
};

const actions = {
    [action.GET_COUNTRIES]({ commit }) {
        auth.getCountries().then(resp => {
            commit(mutation.SET_COUNTRIES, resp);
        });
    },
    [action.GET_STATES]({ commit }, payload) {
        auth.getStates(payload).then(resp => {
            commit(mutation.SET_STATES, resp);
        });
    },
    [action.GET_CITIES]({ commit }, payload) {
        auth.getCities(payload).then(resp => {
            commit(mutation.SET_CITIES, resp);
        });
    },
    [action.GET_LOCAL_GOVERNMENTS]({ commit }, payload) {
        auth.getLocalGovernments(payload).then(resp => {
            commit(mutation.SET_LOCAL_GOVERNMENTS, resp);
        });
    },
    [action.GET_INDUSTRIES]({ commit }) {
        auth.getIndustries().then(resp => {
            commit(mutation.SET_INDUSTRIES, resp.data);
        });
    },
    [action.VERIFY_EMAIL]({ commit, getters }) {
        const id = getters.getUser.id;
        return auth
            .verifyEmail(id)
            .then(resp => {
                if (resp.status == 200) {
                    return true;
                }
            })
    }
};

const mutations = {
    [mutation.SET_COUNTRIES](state, payload) {
        state.countries = payload;
    },

    [mutation.SET_STATES](state, payload) {
        state.states = payload;
    },

    [mutation.SET_CITIES](state, payload) {
        state.cities = payload;
    },
    [mutation.SET_LOCAL_GOVERNMENTS](state, payload) {
        state.localGovernments = payload;
    },

    [mutation.SET_INDUSTRIES](state, payload) {
        state.industries = payload;
    },

    [mutation.SET_EMAIL_ID](state, payload) {
        state.verifyEmailId = payload;
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};
