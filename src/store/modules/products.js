import * as action from '../actionTypes';
import * as mutation from '../mutationTypes';
import product from '../config/products';

const state = {
    allProducts: [],
    vendorProducts: [],
    singleProduct: {},
    allTags:[],
};

const getters = {
    getVendorProducts: state => {
        return state.vendorProducts;
    },
    getAllProducts: state => {
        return state.allProducts;
    },
    getSingleProduct: state => {
        return state.singleProduct;
    },
    getProductTags: state => {
        return state.allTags;
    },

};

const actions = {
    [action.GET_ALL_PRODUCTS]({ commit }) {
        return product.getAllProducts().then(resp => {
            if (resp.type == 'error') {
                return resp
            }
            else {
                commit(mutation.SET_ALL_PRODUCTS, resp);
            }
        });
    },
    [action.GET_SINGLE_PRODUCT]({ commit }, payload) {
        return product.getSingleProduct(payload).then(resp => {
            if (resp.type == 'error') {
                return resp
            }
            else {
                commit(mutation.SET_SINGLE_PRODUCT, resp);
            }
        });
    },
    [action.GET_VENDOR_PRODUCTS]({ commit, dispatch }) {
        return product.getVendorProducts().then(resp => {
            if (resp.type == 'error') {
                return resp
            }
            else {
                commit(mutation.SET_VENDOR_PRODUCTS, resp);
            }
        });
    },
    [action.CREATE_PRODUCT]({ commit, dispatch }, payload) {
        return product.createProduct(payload).then(resp => {
            if (resp.type === 'error') {
                return resp
            }
            else {
                dispatch(action.GET_VENDOR_PRODUCTS);
                return 'success';
            }
        });
    },
    [action.DELETE_PRODUCT]({ dispatch }, payload) {
        return product.deleteProduct(payload).then(resp => {
            if (resp.type == 'error') {
                return resp
            }
            else {
                dispatch(action.GET_VENDOR_PRODUCTS);
                return resp.status == 'success' ? true : false;
            }
        });
    },
    [action.UPDATE_PRODUCT]({ dispatch }, payload) {
        return product.updateProduct(payload).then(resp => {
            if (resp.type == 'error') {
                return resp
            }
            else {
                dispatch(action.GET_VENDOR_PRODUCTS);
                return resp.status == 'success' ? true : false;
            }
        });
    },
    [action.DELETE_PRODUCT_IMAGE]({ dispatch }, payload) {
        return product.deleteProductImage(payload).then(resp => {
            if (resp.type == 'error') {
                return resp
            }
            else {
                dispatch(action.GET_VENDOR_PRODUCTS);
                return resp.status == 'success' ? true : false;
            }
        });
    },
    [action.ADD_PRODUCT_IMAGES]({ dispatch }, payload) {
        return product.uploadProductImages(payload).then(resp => {
            if (resp.type == 'error') {
                return resp
            }
            else {
                return resp.status == 'success' ? true : false;
            }
        });
    },
    [action.GET_PRODUCT_TAGS]({ commit }) {
        return product.getProductTags().then(resp => {
            if (resp.type == 'error') {
                return resp
            }
            else {
                commit(mutation.SET_PRODUCT_TAGS, resp);
            }
        });
    },
    
};

const mutations = {
    [mutation.SET_VENDOR_PRODUCTS](state, payload) {
        state.vendorProducts = payload;
    },
    [mutation.SET_PRODUCT_TAGS](state, payload) {
        state.allTags = payload;
    },
    
};

export default {
    state,
    getters,
    mutations,
    actions
};
