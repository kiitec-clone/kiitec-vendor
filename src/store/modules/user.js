import * as action from '../actionTypes';
import * as mutation from '../mutationTypes';
import user from '../config/user';
import APIService from '../../services/APIService';
import AuthService from '../../services/AuthService';
import UserService from '../../services/UserService';

const state = {
    authUser: {},
    vendor: {},
    onboard: false,
    transactions: {}
};

const getters = {
    getUser: state => {
        return state.authUser;
    },
    getVendor: state => {
        return state.vendor;
    },
    getOnboardStatus: state => {
        return state.onboard;
    },
    authUserIsVerified: state => {
        return !!state.authUser.email_verified_at;
    },
    getTransactions: state => {
        return state.transactions;
    }
};

const actions = {
    [action.UPDATE_VENDOR]({ commit, dispatch }, payload) {
        return user
            .updateUser(payload)
            .then(resp => {
                if (resp.type == 'error') {
                    return resp
                } else {
                    dispatch(action.GET_VENDOR);
                    return resp.message == 'Vendor Updated' ? true : false;
                }
            })
    },
    async [action.GET_USER]({ commit }) {
        try {
            const user = await AuthService.getAuthUser();
            commit(mutation.SET_USER, user);

            return user;
        } catch (e) {
            return null;
        }
    },
    async [action.GET_VENDOR]({ commit }) {
        try {
            const vendor = await AuthService.getAuthVendor();
            commit(mutation.SET_VENDOR, vendor);

            return vendor;
        } catch (e) {
            return null;
        }
    },
    async [action.GET_ONBOARD_STATUS]({}, payload) {
        return await UserService.isBoarded();
    },
    [action.GET_ONBOARD_COMPLETE]({ }, payload) {
        return user.completeOnboard(payload)
    },
    [action.VENDOR_ONBOARDING]({ }, payload) {
        return user
            .vendorOnboard(payload)
            .then(resp => {
                return resp;
            })
    },
    [action.VENDOR_ONBOARDING_UPDATE]({ }, payload) {
        return user
            .vendorOnboardUpdate(payload)
            .then(resp => {
                return resp;
            })
    },
    async [action.GET_TRANSACTIONS]({commit} ) {
       const transactions = await UserService.transactions();
       window.console.log(transactions);
       commit(mutation.SET_TRANSACTIONS, transactions);

    }
};

const mutations = {
    [mutation.SET_USER](state, payload) {
        state.authUser = payload;
    },
    [mutation.SET_VENDOR](state, payload) {
        state.vendor = payload;
    },
    [mutation.SET_ONBOARD_STATUS](state, payload) {
        state.onboard = payload;
    },
    [mutation.SET_TRANSACTIONS](state, payload) {
        state.transactions = payload;
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};
