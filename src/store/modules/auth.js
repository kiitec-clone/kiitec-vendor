import * as action from '../actionTypes';
import * as mutation from '../mutationTypes';
import axios from '../../config'

import auth from '../config/auth';
import router from '../../router/router';

const state = {
  token: localStorage.getItem('token') || '',
  verifyEmailId: '',
  countries: [],
  states: [],
  cities: [],
  industries: [],
  lgas: [],
  storeLgas: []
};

const getters = {
  getToken: state => {
    return state.token;
  },
  getEmailId: state => {
    return state.verifyEmailId;
  },
  getCountries: state => {
    return state.countries;
  },
  getStates: state => {
    return state.states;
  },
  getCities: state => {
    return state.cities;
  },
  getIndustries: state => {
    return state.industries;
  },
  getLGAS: state => {
    return state.lgas;
  },
  getStoreLGAS: state => {
    return state.storeLgas;
  }
};

const actions = {
  [action.REGISTER]({ commit }, payload) {
    return auth
      .register(payload)
      .then(resp => {
        if (resp.type == 'error') {
          return resp
        } else if (resp.message === 'Vendor Created') {
          commit(mutation.TOKEN, resp.data.token);
          localStorage.setItem('token', resp.data.token);
          axios.defaults.headers.common.Authorization = `Bearer ${resp.data.token}`;
          return true;
        }
        return;
      })
  },
  oldLogin({ commit }, payload) {
    return auth.login(payload).then(resp => {
      if (resp.type === 'error') {
        return resp
      } else {
        commit(mutation.SET_USER, resp.data.user);
        commit(mutation.TOKEN, resp.data.token);
        localStorage.setItem('token', resp.data.token);
        axios.defaults.headers.common.Authorization = `Bearer ${resp.data.token}`;
        return resp.data.user;
      }
    });
  },
  oldLogout({ commit }) {
    auth.logout();
    router.push('/login');
  },
  [action.GET_COUNTRIES]({ commit }) {
    auth.getCountries().then(resp => {
      commit(mutation.SET_COUNTRIES, resp);
    });
  },
  [action.GET_STATES]({ commit }, payload) {
    auth.getStates(payload).then(resp => {
      commit(mutation.SET_STATES, resp);
    });
  },
  [action.GET_CITIES]({ commit }, payload) {
    auth.getCities(payload).then(resp => {
      commit(mutation.SET_CITIES, resp);
    });
  },
  [action.GET_INDUSTRIES]({ commit }) {
    auth.getIndustries().then(resp => {
      commit(mutation.SET_INDUSTRIES, resp.data);
    });
  },
  [action.VERIFY_EMAIL]({ commit, getters }) {
    const id = getters.getUser.id;
    return auth
      .verifyEmail(id)
      .then(resp => {
        if (resp.status == 200) {
          return true;
        }
      })
  }
};

const mutations = {
  [mutation.TOKEN](state, payload) {
    state.token = payload;
  },

  [mutation.SET_COUNTRIES](state, payload) {
    state.countries = payload;
  },

  [mutation.SET_STATES](state, payload) {
    state.states = payload;
  },

  [mutation.SET_CITIES](state, payload) {
    state.cities = payload;
  },

  [mutation.SET_INDUSTRIES](state, payload) {
    state.industries = payload;
  },

  [mutation.SET_EMAIL_ID](state, payload) {
    state.verifyEmailId = payload;
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
