import TokenService from '../../services/TokenService';
import {GET_ONBOARD_STATUS, LOGIN, LOGOUT, REGISTER} from '../actionTypes';
import AuthService from '../../services/AuthService';
import router from '../../router/router';
import AuthenticationError from '../../classes/AuthenticationError';
import {SET_USER} from '../mutationTypes';

const state = {
	authenticating: false,
	accessToken: TokenService.getToken(),
	authenticationErrorCode: 0,
	authenticationErrorMessage: '',
	errorData: {},
};

const actions = {
	async [LOGIN]({ commit, dispatch }, {email, password}) {
		try {
			const data = await AuthService.login(email, password);
			commit('loginSuccess', data.token);
			commit('loginError', {errorCode: 0, errorMessage: '', errorData: {}});
			commit(SET_USER, data.user, { root: true });

			if (await dispatch(GET_ONBOARD_STATUS)) {
				router.push(router.history.current.query.redirect || '/dashboard');
			} else {
				router.push({ name: 'gettingstarted' });
			}

			return true;
		} catch (e) {
			if (e instanceof AuthenticationError) {
				commit('loginError', {errorCode: e.errorCode, errorMessage: e.message, errorData: e.data});
			}

			return false;
		}

	},
	async [REGISTER]({ commit, dispatch }, data) {
		commit('loginRequest');

		try {
			const userData = await AuthService.register(data);

			commit('loginSuccess', userData.token);
			commit(SET_USER, userData.user, { root: true });

			commit('loginError', {errorCode: 0, errorMessage: '', errorData: {}});

			if (await dispatch(GET_ONBOARD_STATUS)) {
				router.push(router.history.current.query.redirect || '/dashboard');
			} else {
				router.push({ name: 'gettingstarted' });
			}
			router.push(router.history.current.query.redirect || '/dashboard');

			return true;
		} catch (e) {
			if (e instanceof AuthenticationError) {
				commit('loginError', {errorCode: e.errorCode, errorMessage: e.message, errorData: e.data});
			}

			return false;
		}
	},
	[LOGOUT]({ commit }, redirect = true) {
		AuthService.logout();
		commit('logoutSuccess');
		if (redirect) {
			router.push('/login');
		}
	},
};

const mutations = {
	loginRequest(state) {
		state.authenticating = true;
		state.authenticationError = '';
		state.authenticationErrorCode = 0;
	},

	loginSuccess(state, accessToken) {
		state.accessToken = accessToken;
		state.authenticating = false;
	},

	loginError(state, { errorCode, errorMessage, errorData }) {
		state.authenticating = false;
		state.authenticationErrorCode = errorCode;
		state.authenticationError = errorMessage;
		state.errorData = errorData;
	},

	logoutSuccess(state) {
		state.accessToken = '';
	},
};

const getters = {
	loggedIn: (state) => {
		return !!state.accessToken;
	},

	authenticationErrorCode: (state) => {
		return state.authenticationErrorCode;
	},

	authenticationError: (state) => {
		return state.authenticationError;
	},

	authenticating: (state) => {
		return state.authenticating;
	},

	loginErrorData: (state) => {
		return state.errorData;
	},
};

export const auth = {
	getters,
	mutations,
	actions,
	state,
};
