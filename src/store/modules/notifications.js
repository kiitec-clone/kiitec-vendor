import APIService from '../../services/APIService';

const state = {
    unreadNotifications: [],
    hasUnreadNotification: false,
};

const actions = {
    async markAllUnreadNotifications({commit}) {
        const response = await APIService.get('/vendor/notifications/read-all');
        commit('hasUnreadNotification', false);
    },
    async getAllUnreadNotifications({commit}) {
        const response = await APIService.get('/vendor/notifications/unread');
        if (response.data.data.length > 0) {
            commit('hasUnreadNotification', true);
        }

        commit('unreadNotifications', response.data.data);
    }
};

const mutations = {
    hasUnreadNotification(state, flag) {
        state.hasUnreadNotification = flag;
    },
    unreadNotifications(state, notifications) {
        state.unreadNotifications = notifications;
    }
};

const getters = {
    hasUnreadNotification(state) {
        return state.hasUnreadNotification;
    },
    unreadNotifications(state) {
        return state.unreadNotifications;
    },
};

export const notifications = {
    actions,
    mutations,
    getters,
    state,
};
