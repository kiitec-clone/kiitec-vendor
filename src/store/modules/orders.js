import * as action from '../actionTypes';
import * as mutation from '../mutationTypes';

import order from '../config/orders';

const state = {
  orders: []
};

const getters = {
  getOrders: state => {
    return state.orders;
  },
};

const actions = {
  [action.GET_ORDERS]({ commit }) {
    return order
      .getOrders()
      .then(resp => {
        if (resp.type == 'error') {
          return resp
        } else {
          commit(mutation.SET_ORDERS, resp.data)
          return true;
        }
      })
  }
};

const mutations = {
  [mutation.SET_ORDERS](state, payload) {
    state.orders = payload;
  },
};

export default {
  state,
  getters,
  mutations,
  actions
};
