import { Validator } from 'vee-validate';
import PhoneNumber from 'awesome-phonenumber';
import APIService from '../services/APIService';

const phoneNumber = {
    getMessage: field => `Phone number is not valid!`,
    validate (value) {
        return new Promise(resolve => {
            let phone = new PhoneNumber(value);
            resolve({ valid: phone.isValid() })
        })
    }
};
Validator.extend('phoneNumber', phoneNumber);

const uniqueEmail = {
    lazy: true,
    getMessage: field => `Email is already taken!`,
    async validate(value) {
        // You might want to check if its a valid email
        // before sending to server...
        const { data } = await APIService.post('/validate/email-unique', { email: value });

        // server response
        if (data.available) {
            return true;
        }

        return {
            valid: false,
            // the data object contents can be used in the message template
            data: {
                error: data.error
            }
        };
    },
    message: `Email is already taken!` // will display the server error message.
};

Validator.extend('uniqueEmail', uniqueEmail);

const uniqueSlug = {
    lazy: true,
    getMessage: field => `Store name is already taken!`,
    async validate(value) {
        // You might want to check if its a valid email
        // before sending to server...
        const { data } = await APIService.post('/validate/store-slug-unique', { slug: value });

        // server response
        if (data.available) {
            return true;
        }

        return {
            valid: false,
            // the data object contents can be used in the message template
            data: {
                error: data.error
            }
        };
    },
};

Validator.extend('uniqueSlug', uniqueSlug);

const uniqueVendorCustomizationName = {
    lazy: true,
    getMessage: field => `You have used this customization name before!`,
    async validate(value) {
        // You might want to check if its a valid email
        // before sending to server...
        const { data } = await APIService.post('/validate/customization-vendor-unique', { name: value });

        // server response
        if (data.available) {
            return true;
        }

        return {
            valid: false,
            // the data object contents can be used in the message template
            data: {
                error: data.error
            }
        };
    },
    message: `You have used this customization name before!` // will display the server error message.
};

Validator.extend('uniqueVendorCustomizationName', uniqueVendorCustomizationName);
