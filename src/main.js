import Vue from 'vue';

import VeeValidate from 'vee-validate';
import ElementUI from 'element-ui';
import VueSweetalert2 from 'vue-sweetalert2';

import 'sweetalert2/dist/sweetalert2.min.css';

import 'element-ui/lib/theme-chalk/index.css';

import moment from 'moment';

import 'babel-polyfill';

import App from './App.vue';
import store from './store';

import './plugins/vuetify';
import router from './router/router';
require('./config.js');
Vue.config.productionTip = false;
import Vuetify from 'vuetify';
import APIService from './services/APIService';
import TokenService from './services/TokenService';
Vue.use(VueSweetalert2);
Vue.use(ElementUI);
Vue.use(VeeValidate);
Vue.use(Vuetify);

Vue.filter('formatNumber', (value) => {
  if (!value) return 0;

  return new Intl.NumberFormat('en-US', {
    minimumFractionDigits: 2,
  }).format(Number(value));
});

Vue.filter('formatDate', (value) => {
  if (!value) return '';
  // return new Date(value).toLocaleDateString('en-GB').replace(/\//g, '-');
  return moment(value).format("DD-MM-YYYY HH:mm");
});

// router.beforeEach((to, from, next) => {
//   const auth = to.matched.some(record => record.meta.auth);
//   if (auth) {
//     next('/login');
//   } else {
//     next();
//   }
// });

APIService.init(process.env.VUE_APP_ROOT_API);

if (TokenService.getToken()) {
  APIService.setAuthHeader();
  APIService.mount401Interceptor();
}

require('./config/validator');
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
