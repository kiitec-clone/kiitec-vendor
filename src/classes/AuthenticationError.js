class AuthenticationError extends Error {
    constructor(errorCode, message, data = null) {
        super();
        Object.setPrototypeOf(this, AuthenticationError.prototype);
        this.name = this.constructor.name;
        this.message = message;
        this.errorCode = errorCode;
        this.data = data;
    }
}

export default AuthenticationError;
