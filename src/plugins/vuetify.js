import Vue from 'vue';

import Vuetify from 'vuetify/lib';
import 'vuetify/src/stylus/app.styl';

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    primary: '#38B4FF',
    // secondary: "#6DBD56",
    accent: '#8c9eff',
    success: '#6DBD56',
    error: '#F97A5D',
    nude: 'BABABA',
    nudelight: 'F2F2F2'
  }
});
