export default {
    beforeRouteEnter(to, from, next) {
        next(vm => vm.getVendorHeaderData());
    },
    methods: {
        async getVendorHeaderData() {
            await this.$store.dispatch('getAllUnreadNotifications');
        }
    },
}
