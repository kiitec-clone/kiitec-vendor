import Vue from 'vue';

const mixin = Vue.mixin({
  data() {
    return {
      genderSelect: ['Men', 'Women', 'Kids', 'Unisex'],
      snackbar: {
        color: '',
        show: false,
        text: ''
      },
      working: false
    };
  },
  methods: {
    capitalize(word) {
      if (word) {
        let arr = word.split('');
        arr[0] = arr[0].toUpperCase();
        return arr.join('');
      }
    },
    isComplete(formData) {
      let complete = true;
      // if (typeof formData == 'object') {
      Object.values(formData).forEach(val => {
        if (val === null || typeof val == 'undefined') complete == false;
        else if (typeof val == 'object' && Object.keys(val).length <= 0)
          complete = false;
        else if (typeof val == 'array' && val.length <= 0) complete = false;
        else if (!val) complete = false;
      });
      return complete;
    }
    // }
  }
});

export default mixin;
