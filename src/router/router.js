import Vue from 'vue';
import routes from './routes.js';
import TokenService from '../services/TokenService';
import UserService from '../services/UserService';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes
});

router.beforeEach(async (to, from, next) => {
    const isPublic = to.matched.some((record) => record.meta.public);
    const onlyWhenLoggedOut = to.matched.some((record) => record.meta.onlyWhenLoggedOut);
    const onBoardingNotRequired = to.matched.some((record) => record.meta.onBoardingNotRequired);
    const allowedWhenNotVerified = to.matched.some((record) => record.meta.allowedWhenNotVerified === false);
    const mustBeApproved = to.matched.some((record) => record.meta.mustBeApproved);
    const loggedIn = !!TokenService.getToken();

    document.title = (to.meta.title || 'Welcome To Kiitec.com')

    if (!isPublic && !loggedIn) {
        return next({
            path: '/login',
            query: { redirect: to.fullPath },
        });
    }
    if (loggedIn && onlyWhenLoggedOut) {
        return next('/dashboard');
    }

    // console.log(allowedWhenNotVerified, loggedIn, isPublic);
    // if (loggedIn && allowedWhenNotVerified === false) {
    //     const user = await UserService.getAuthUser();
    //     if (!user.isVerified) {
    //         return next('/verify');
    //     }
    // }

    if (loggedIn && !onBoardingNotRequired) {
        const isBoarded = await UserService.isBoarded();
        if (!isBoarded) return next({ name: 'gettingstarted' });
    }

    if (loggedIn && mustBeApproved) {
        const isApproved = await UserService.isApproved();
        if (!isApproved) return next({ name: 'approvalStatus' });
    }

    next();
});

export default router;
