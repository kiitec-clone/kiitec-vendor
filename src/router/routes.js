import Test from '../components/Test';

const Login = () => import('@/pages/auth/Login');
const Register = () => import('@/pages/auth/Register');
const Home = () => import('@/pages/home');
const Pagenotfound = () => import('@/pages/pagenotfound');
// const Dashboard = () => import('@/pages/dashboard/Index');
const Dashboard = () => import('@/components/SideBar');
const Dashboardhome = () => import('@/pages/dashboard/Home');
const Products = () => import('@/pages/dashboard/Products');
const Orders = () => import('@/pages/dashboard/Orders');
const Wallet = () => import('@/pages/dashboard/Wallet');
const Tracker = () => import('@/pages/dashboard/Tracker');
const Profile = () => import('@/pages/dashboard/Profile');
const FAQ = () => import('@/pages/Faq');
const Contact = () => import('@/pages/Contact');
const Action = () => import('@/pages/Action');
const About = () => import('@/pages/About');
const Template = () => import('@/pages/OrderTemplate');
const CustomizationFeatures = () =>
  import('@/pages/dashboard/CustomizationFeatures');
const CustomizationPage = () => import('@/pages/dashboard/Customization');

const GettingStarted = () => import('@/components/GettingStarted');

// const CustomizeIndex = () => import('@/pages/customization/Index');
// const CustomizeHome = () => import('@/pages/customization/Home');

export default [
  {
    path: '/',
    component: Home,
    name: 'home',
    meta: {
      title: 'Home | Kiitec.com',
      public: true,
      onlyWhenLoggedOut: false
    }
  },
  {
    path: '/login',
    component: Login,
    name: 'login',
    meta: {
      title: 'Login | Kiitec.com',
      public: true,
      onlyWhenLoggedOut: true
    }
  },
  {
    path: '/register',
    component: Register,
    name: 'register',
    meta: {
      title: 'Register | Kiitec.com',
      public: true,
      onlyWhenLoggedOut: true
    }
  },
  {
    path: '/faq',
    name: 'faq',
    component: FAQ,
      meta: {
     title: 'FAQ | Kiitec.com',
      public: true
    }
  },
  {
    path: '/welcome',
    name: 'gettingstarted',
    component: GettingStarted,
    meta: {
      title: 'Onboarding | Kiitec.com',
      onBoardingNotRequired: true
    }
  },
  {
    path: '/test',
    name: 'test',
    component: Test
  },
  {
    path: '/dashboard',
    component: Dashboard,
    children: [
      {
        path: '',
        name: 'dashboard',
        component: Dashboardhome,
        meta: {
            title: 'Dashboard | Kiitec.com',
        },
      },
      {
        path: 'products',
        name: 'products',
        component: Products,
        meta: {
            title: 'Products | Kiitec.com',
        },
      },
      {
        path: 'customization',
        name: 'customization',
        component: CustomizationPage,
        meta: {
            title: 'Customization | Kiitec.com',
        },
      },
      {
        path: 'customization/:id',
        name: 'customization-features',
        component: CustomizationFeatures,
        meta: {
            title: 'Customization Features | Kiitec.com',
        },
      },
      {
        path: 'orders',
        name: 'orders',
        component: Orders,
        meta: {
            title: 'Orders | Kiitec.com',
        },
      },
      {
        path: 'wallet',
        name: 'wallet',
        component: Wallet,
        meta: {
            title: 'Wallet | Kiitec.com',
        },
      },
      {
        path: 'tracker',
        name: 'tracker',
        component: Tracker,
        meta: {
            title: 'Tracker | Kiitec.com',
        },
      },
      {
        path: 'profile',
        name: 'profile',
        component: Profile,
        meta: {
            title: 'Profile | Kiitec.com',
        },
      }
    ],
    meta: {
      mustBeApproved: true
    }
  },
  // {
  //     path: '/dashboard/customize',
  //     component: CustomizeIndex,
  //     children: [
  //         {
  //             path: '',
  //             name: 'customize',
  //             component: CustomizeHome
  //         }]
  // },
  {
    path: '*',
    component: Pagenotfound
  },
  {
    path: '/contact',
    name: 'contact',
    component: Contact
  },
  {
    path: '/about',
    name: 'aboutUs',
    component: About
  },
  {
    path: '/approval-status',
    name: 'approvalStatus',
    component: Action
  },
  {
    path: '/template',
    name: 'email-template',
    component: Template
  }
];
