import axios from 'axios';

export const API_URL = process.env.VUE_APP_ROOT_API;

export const X_API_KEY = "";

export const api = axios.create({
  baseURL: API_URL,
  withCredentials: false
});

api.defaults.headers.common['X-api-key'] = X_API_KEY;
api.defaults.headers.common['Content-Type'] = 'application/json';
api.defaults.headers.common['Accept'] = 'application/json';
api.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

export const API_KEY = {'X-api-key': X_API_KEY};

export default api;
